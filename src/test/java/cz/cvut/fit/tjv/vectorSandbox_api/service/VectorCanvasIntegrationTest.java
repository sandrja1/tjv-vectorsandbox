package cz.cvut.fit.tjv.vectorSandbox_api.service;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Editor;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.EditorRepository;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.ShapeRepository;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.VectorCanvasRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.util.ArrayList;
import java.util.Optional;

@SpringBootTest
public class VectorCanvasIntegrationTest {

    @Autowired
    private EditorRepository editorRepository;
    @Autowired
    private VectorCanvasRepository vectorCanvasRepository;
    @Autowired
    private ShapeRepository shapeRepository;
    @Autowired
    private VectorCanvasServiceImpl canvasService;

    Editor editor = new Editor();

    @BeforeEach
    void setUp()
    {
        editorRepository.deleteAll();
        vectorCanvasRepository.deleteAll();
        shapeRepository.deleteAll();
        editor.setUsername("test");

        editor.setEditedByMe(new ArrayList<>());

        editorRepository.save(editor);

        VectorCanvas vectorCanvas = new VectorCanvas();
        vectorCanvas.setWidth(300);
        vectorCanvas.setHeight(200);
        vectorCanvas.setName("testCanvas");
        vectorCanvas.setEditors(new ArrayList<>());
        vectorCanvas.setShapes(new ArrayList<>());

        vectorCanvas.getEditors().add(editor);
        editor.getEditedByMe().add(vectorCanvas);

        vectorCanvasRepository.save(vectorCanvas);
    }

    @Test
    @Transactional
    public void editorAddShapeToHisCanvas ( )
    {
        Shape shape = new Shape();

        VectorCanvas editorCanvas = (VectorCanvas) editor.getEditedByMe().toArray()[0];

        shape.setId(1L);
        shape.setDefinition("line 10 110 50 150 5 black");
        shapeRepository.save(shape);

        editorCanvas.setShapes(new ArrayList<>());

        canvasService.addShape(editorCanvas.getId(),shape.getId());

        Assertions.assertFalse(vectorCanvasRepository.findById(editorCanvas.getId()).isEmpty());

        Assertions.assertEquals(1,vectorCanvasRepository.findById(editorCanvas.getId()).get().getShapes().size());

        Assertions.assertTrue(vectorCanvasRepository.findById(editorCanvas.getId()).get().getShapes().contains(shape));
    }

    @Test
    @Transactional
    public void editorDeleteNonExistingShapeFromHisCanvas ( )
    {
        VectorCanvas editorCanvas = (VectorCanvas) editor.getEditedByMe().toArray()[0];

        var shapesCount = editorCanvas.getShapes().size();

        Assertions.assertThrows(IllegalArgumentException.class, () -> canvasService.deleteShape(editorCanvas.getId(),76L));

        Assertions.assertEquals(shapesCount, editorCanvas.getShapes().size());
    }
}
