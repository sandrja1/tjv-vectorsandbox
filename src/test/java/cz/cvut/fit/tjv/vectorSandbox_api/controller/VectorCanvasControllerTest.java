package cz.cvut.fit.tjv.vectorSandbox_api.controller;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Editor;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import cz.cvut.fit.tjv.vectorSandbox_api.service.EditorService;
import cz.cvut.fit.tjv.vectorSandbox_api.service.ShapeService;
import cz.cvut.fit.tjv.vectorSandbox_api.service.VectorCanvasService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(VectorCanvasController.class)
class VectorCanvasControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private VectorCanvasService vectorCanvasService;
    @MockBean
    private ShapeService shapeService;
    @MockBean
    private EditorService editorService;

    @Test
    void create() throws Exception {
        VectorCanvas vectorCanvas = new VectorCanvas();
        vectorCanvas.setName("test1");
        vectorCanvas.setWidth(450L);
        vectorCanvas.setHeight(666L);
        Mockito.when(vectorCanvasService.create(vectorCanvas)).thenReturn(vectorCanvas);

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/canvas")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\" : \"test1\", \"width\" : 450, \"height\": 666 }")
        ).andExpect(
                MockMvcResultMatchers.status().isOk()
        ).andExpect(
                MockMvcResultMatchers.jsonPath("$.id", Matchers.is("test1"))
        );
    }
}