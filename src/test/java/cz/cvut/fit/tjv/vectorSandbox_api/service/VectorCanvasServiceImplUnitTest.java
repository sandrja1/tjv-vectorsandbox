package cz.cvut.fit.tjv.vectorSandbox_api.service;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.VectorCanvasRepository;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.ShapeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Optional;

@SpringBootTest
public class VectorCanvasServiceImplUnitTest {

    @Autowired
    private VectorCanvasServiceImpl canvasService ;

    @MockBean
    private VectorCanvasRepository canvasRepository;

    @MockBean
    private ShapeRepository shapeRepository;

    @Test
    void addDifferentShapes ( )
    {
        var canvas = new VectorCanvas();
        var shape1 = new Shape();
        var shape2 = new Shape();

        canvas.setName("sandbox");
        canvas.setWidth(300);
        canvas.setHeight(300);
        canvas.setShapes(new ArrayList<>());

        shape1.setId(1L);
        shape1.setDefinition("line 10 110 50 150 5 black");
        shape2.setId(2L);
        shape2.setDefinition("line 20 30 50 70 3 red");

        Mockito.when(canvasRepository.findById(canvas.getId())).thenReturn(Optional.of(canvas));
        Mockito.when(shapeRepository.findById(shape1.getId())).thenReturn((Optional.of(shape1)));
        Mockito.when(shapeRepository.findById(shape2.getId())).thenReturn((Optional.of(shape2)));

        canvasService.addShape(canvas.getId(),shape1.getId());
        canvasService.addShape(canvas.getId(),shape2.getId());

        Assertions.assertTrue(canvas.getShapes().contains(shape1));
        Assertions.assertTrue(canvas.getShapes().contains(shape2));
        Assertions.assertEquals(2, canvas.getShapes().size());

        Mockito.verify(canvasRepository, Mockito.atLeast(2)).save(canvas);
    }

    @Test
    void addExistingShape ( )
    {
        var canvas = new VectorCanvas();
        var shape1 = new Shape();
        var shape2 = new Shape();

        canvas.setName("sandbox");
        canvas.setWidth(300);
        canvas.setHeight(300);
        canvas.setShapes(new ArrayList<>());

        shape1.setId(1L);
        shape1.setDefinition("line 10 110 50 150 5 black");
        shape2.setId(2L);
        shape2.setDefinition("line 10 110 50 150 5 black");

        Mockito.when(canvasRepository.findById(canvas.getId())).thenReturn(Optional.of(canvas));
        Mockito.when(shapeRepository.findById(shape1.getId())).thenReturn((Optional.of(shape1)));
        Mockito.when(shapeRepository.findById(shape2.getId())).thenReturn((Optional.of(shape2)));

        canvasService.addShape(canvas.getId(),shape1.getId());

        Assertions.assertThrows(IllegalArgumentException.class, () -> canvasService.addShape(canvas.getId(),shape1.getId()));
        Assertions.assertTrue(canvas.getShapes().contains(shape1));
        Assertions.assertFalse(canvas.getShapes().contains(shape2));
        Assertions.assertEquals(1, canvas.getShapes().size());


        Mockito.verify(canvasRepository, Mockito.atLeast(1)).save(canvas);
        Mockito.verify(canvasRepository, Mockito.times(2)).findById(canvas.getId());
        Mockito.verifyNoMoreInteractions(canvasRepository);
    }
}
