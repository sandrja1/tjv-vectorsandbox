-- Remove conflicting tables
DROP TABLE IF EXISTS editor CASCADE;
DROP TABLE IF EXISTS shape CASCADE;
DROP TABLE IF EXISTS vector_canvas CASCADE;
DROP TABLE IF EXISTS vector_canvas_editor CASCADE;
-- End of removing

CREATE TABLE editor (
                        username VARCHAR(256) NOT NULL,
                        realname VARCHAR(256)
);
ALTER TABLE editor ADD CONSTRAINT pk_editor PRIMARY KEY (username);

CREATE TABLE shape (
                       idshape SERIAL NOT NULL ,
                       outputcanvas VARCHAR(256),
                       definition VARCHAR(256) NOT NULL
);
ALTER TABLE shape ADD CONSTRAINT pk_shape PRIMARY KEY (idshape);

CREATE TABLE vector_canvas (
                               name VARCHAR(256) NOT NULL,
                               width INTEGER NOT NULL,
                               height INTEGER NOT NULL
);
ALTER TABLE vector_canvas ADD CONSTRAINT pk_vector_canvas PRIMARY KEY (name);

CREATE TABLE vector_canvas_editor (
                                      canvasname VARCHAR(256) NOT NULL,
                                      editorname VARCHAR(256) NOT NULL
);
ALTER TABLE vector_canvas_editor ADD CONSTRAINT pk_vector_canvas_editor PRIMARY KEY (canvasname, editorname);

ALTER TABLE shape ADD CONSTRAINT fk_shape_vector_canvas FOREIGN KEY (outputcanvas) REFERENCES vector_canvas (name) ON DELETE CASCADE;

ALTER TABLE vector_canvas_editor ADD CONSTRAINT fk_vector_canvas_editor_vector_ FOREIGN KEY (canvasname) REFERENCES vector_canvas (name) ON DELETE CASCADE;
ALTER TABLE vector_canvas_editor ADD CONSTRAINT fk_vector_canvas_editor_editor FOREIGN KEY (editorname) REFERENCES editor (username) ON DELETE CASCADE;


INSERT INTO editor (username, realname) VALUES ('edi', 'lol');
INSERT INTO editor (username, realname) VALUES ('edi2', 'lol2');

INSERT INTO vector_canvas (name, width, height) VALUES ('testCanvas', 300, 200);
INSERT INTO vector_canvas (name, width, height) VALUES ('testCanvas2', 500, 300);
INSERT INTO vector_canvas (name, width, height) VALUES ('testCanvas10', 400, 100);

INSERT INTO shape (idshape, outputcanvas, definition) VALUES (1, 'testCanvas', 'line 10 110 50 150 black 5');
INSERT INTO shape (idshape, outputcanvas, definition) VALUES (2, 'testCanvas', 'circle 25 75 20 transparent red 5');
INSERT INTO shape (idshape, outputcanvas, definition) VALUES (3, 'testCanvas2', 'rectangle 30 30 30 30 #000000 #000000 2');
INSERT INTO shape (idshape, outputcanvas, definition) VALUES (4, 'testCanvas10', 'rectangle 60 60 20 20 blue #000000 2');
SELECT setval('shape_idshape_seq', 10, true);

INSERT INTO vector_canvas_editor (canvasname, editorname) VALUES ('testCanvas', 'edi');
INSERT INTO vector_canvas_editor (canvasname, editorname) VALUES ('testCanvas2', 'edi');
INSERT INTO vector_canvas_editor (canvasname, editorname) VALUES ('testCanvas2', 'edi2');
INSERT INTO vector_canvas_editor (canvasname, editorname) VALUES ('testCanvas10', 'edi2');
