package cz.cvut.fit.tjv.vectorSandbox_api.controller;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import cz.cvut.fit.tjv.vectorSandbox_api.service.ShapeService;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping("/shape")
public class ShapeController {

    private ShapeService shapeService;

    public ShapeController(ShapeService shapeService) { this.shapeService = shapeService;}

    @Operation(summary = "Returns all shapes that appear in any canvas")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Shapes found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Shape.class)) }),
    })
    @GetMapping
    public Iterable<Shape> readAll() { return shapeService.readAll();}
}


