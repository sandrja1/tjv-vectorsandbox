package cz.cvut.fit.tjv.vectorSandbox_api.domain;

public interface EntityWithId<ID> {
    ID getId();
}
