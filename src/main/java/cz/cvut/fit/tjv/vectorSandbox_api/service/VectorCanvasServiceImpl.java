package cz.cvut.fit.tjv.vectorSandbox_api.service;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.VectorCanvasRepository;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.ShapeRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@Transactional
public class VectorCanvasServiceImpl extends CrudServiceImpl<VectorCanvas, String >implements VectorCanvasService
{
    private VectorCanvasRepository canvasRepository;

    private ShapeRepository shapeRepository;

    public VectorCanvasServiceImpl(VectorCanvasRepository canvasRepository, ShapeRepository shapeRepository)
    {
        this.canvasRepository = canvasRepository;
        this.shapeRepository = shapeRepository;
    }

    @Override
    public void addShape (String canvasId, long shapeId)
    {
        Optional<VectorCanvas> optCanvas = canvasRepository.findById(canvasId);
        Optional<Shape> optShape = shapeRepository.findById(shapeId);

        if ( optCanvas.isEmpty() || optShape.isEmpty())
            throw  new IllegalArgumentException("invalid ID");

        VectorCanvas canvas = optCanvas.get();
        Shape shape = optShape.get();

        if ( canvas.getShapes().stream().anyMatch(s -> s.getDefinition().equals(shape.getDefinition())))
            throw new IllegalArgumentException ( "Canvas already contains shape with exact definition!");

        shape.setOutputCanvas(canvas);
        canvas.getShapes().add(shape);

        canvasRepository.save(canvas);
    }

    @Override
    public void deleteShape (String canvasId, long shapeId)
    {
        Optional<VectorCanvas> optCanvas = canvasRepository.findById(canvasId);
        Optional<Shape> optShape = shapeRepository.findById(shapeId);

        if ( optCanvas.isEmpty() || optShape.isEmpty())
            throw  new IllegalArgumentException("invalid ID");

        VectorCanvas canvas = optCanvas.get();
        Shape shape = optShape.get();

        if(!canvas.getShapes().contains(shape))
            throw new IllegalArgumentException ( "Cannot delete non-existing shape!");

        canvas.getShapes().remove(shape);
        shapeRepository.deleteById(shapeId);
    }

    @Override
    protected CrudRepository<VectorCanvas,String> getRepository() {return  canvasRepository;}
}
