package cz.cvut.fit.tjv.vectorSandbox_api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;

@Entity
@Table(name = "vector_canvas")
public class VectorCanvas implements EntityWithId<String> {
    @Id
    private String name = "";

    private long width;

    private long height;

    @OneToMany(mappedBy = "outputCanvas" )
    private Collection < Shape > shapes;

    @ManyToMany(mappedBy = "editedByMe")
    @JsonIgnore
    private Collection <Editor> editors;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof VectorCanvas c) {
            return this.name.equals(c.name);
        } else
            return false;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public String getId() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getWidth() {
        return width;
    }

    public void setWidth(long width) {
        this.width = width;
    }

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public Collection<Shape> getShapes() {
        return shapes;
    }

    public void setShapes(Collection<Shape> shapes) {
        this.shapes = shapes;
    }

    public Collection<Editor> getEditors() {
        return editors;
    }

    public void setEditors(Collection<Editor> editors) {
        this.editors = editors;
    }

}
