package cz.cvut.fit.tjv.vectorSandbox_api.repository;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import org.springframework.data.repository.CrudRepository;

public interface VectorCanvasRepository extends CrudRepository<VectorCanvas, String> {
}
