package cz.cvut.fit.tjv.vectorSandbox_api.service;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Editor;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.EditorRepository;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.ShapeRepository;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.VectorCanvasRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
@Transactional
public class EditorServiceImpl extends CrudServiceImpl<Editor, String> implements EditorService {

    private EditorRepository editorRepository;

    private VectorCanvasRepository vectorCanvasRepository;

    private ShapeRepository shapeRepository;

    public EditorServiceImpl ( EditorRepository editorRepository, VectorCanvasRepository vectorCanvasRepository, ShapeRepository shapeRepository) {
        this.editorRepository = editorRepository;
        this.vectorCanvasRepository = vectorCanvasRepository;
        this.shapeRepository = shapeRepository;
    }

    @Override
    public void addCanvas(String editorId, String canvasId) {

        var vectorCanvas = vectorCanvasRepository.findById(canvasId).get();
        var editor = editorRepository.findById(editorId).get();

        if(editor.getEditedByMe().contains(vectorCanvas))
            throw new IllegalArgumentException();

        editor.getEditedByMe().add(vectorCanvas);
        update(editorId,editor);
    }

    @Override
    public void removeCanvas (String editorId, String canvasId)
    {
        var vectorCanvas = vectorCanvasRepository.findById(canvasId).get();
        var currentEditor = editorRepository.findById(editorId).get();
        currentEditor.getEditedByMe().remove(vectorCanvas);
        update(editorId,currentEditor);

        var editing = getEditorsNamesThatEditCanvas(vectorCanvas);

        if(!editing.isEmpty())
            return;

        for ( var shape : vectorCanvas.getShapes())
            shapeRepository.deleteById(shape.getId());

        vectorCanvasRepository.deleteById(canvasId);
    }

    @Override
    public Collection<String> getEditorsNamesThatEditCanvas(VectorCanvas canvas)
    {
        return editorRepository.getEditorsNamesThatEditCanvas(canvas);
    }

    @Override
    public Collection<String> getEditorsNamesThatNotEditCanvas(VectorCanvas canvas)
    {
        return editorRepository.getEditorsNamesThatNotEditCanvas(canvas);
    }

    @Override
    protected CrudRepository<Editor, String> getRepository(){return  editorRepository;}
}
