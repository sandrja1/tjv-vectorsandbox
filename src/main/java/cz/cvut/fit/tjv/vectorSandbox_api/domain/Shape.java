package cz.cvut.fit.tjv.vectorSandbox_api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "shape")
public class Shape implements EntityWithId<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idshape")
    private Long idShape;

    private String definition;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "outputcanvas")
    private VectorCanvas outputCanvas ;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shape shape = (Shape) o;

        return Objects.equals(idShape, shape.idShape);
    }

    @Override
    public int hashCode() {
        return idShape != null ? idShape.hashCode() : 0;
    }

    @Override
    public Long getId() {
        return idShape;
    }

    public void setId(Long idShape) {
        this.idShape = idShape;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public VectorCanvas getOutputCanvas() {
        return outputCanvas;
    }

    public void setOutputCanvas(VectorCanvas outputCanvas) {
        this.outputCanvas = outputCanvas;
    }
}
