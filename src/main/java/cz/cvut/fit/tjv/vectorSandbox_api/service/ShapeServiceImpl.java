package cz.cvut.fit.tjv.vectorSandbox_api.service;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import cz.cvut.fit.tjv.vectorSandbox_api.repository.ShapeRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class ShapeServiceImpl extends CrudServiceImpl<Shape, Long> implements ShapeService {

    private ShapeRepository shapeRepository;

    public ShapeServiceImpl (ShapeRepository shapeRepository) {
        this.shapeRepository = shapeRepository;
    }

    @Override
    protected CrudRepository<Shape, Long> getRepository(){return  shapeRepository;}
}
