package cz.cvut.fit.tjv.vectorSandbox_api.repository;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import org.springframework.data.repository.CrudRepository;

public interface ShapeRepository extends CrudRepository<Shape,Long> {
}
