package cz.cvut.fit.tjv.vectorSandbox_api.controller;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Editor;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import cz.cvut.fit.tjv.vectorSandbox_api.service.EditorService;
import cz.cvut.fit.tjv.vectorSandbox_api.service.ShapeService;
import cz.cvut.fit.tjv.vectorSandbox_api.service.VectorCanvasService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.awt.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/editor")
public class EditorController {

    private EditorService editorService;

    private VectorCanvasService vectorCanvasService;

    public EditorController(EditorService editorService, VectorCanvasService vectorCanvasService)
    {
        this.editorService = editorService;
        this.vectorCanvasService = vectorCanvasService;
    }

    @Operation(summary = "Creates a new editor")
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Editor created", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Editor.class)) }),
            @ApiResponse(responseCode = "400", description = "Editor with same username already exists"),
    })
    public Editor create (@RequestBody Editor editor )
    {
        if(editorService.readById(editor.getId()).isPresent())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        return editorService.create(editor);
    }

    @Operation(summary = "Allows to change editor's realname")
    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Editor's realname changed"),
            @ApiResponse(responseCode = "404", description = "invalid ID")
    })
    public void updateRealName (@PathVariable String id, @RequestBody Editor e ) {
        if(editorService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        var editor = editorService.readById(id).get();
        editor.setRealName(e.getRealName());

        editorService.update(id,e);
    }

    @Operation(summary = "Deletes existing editor")
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Editor removed"),
            @ApiResponse(responseCode = "404", description = "invalid ID")
    })
    public void deleteById(@PathVariable String id) {
        try {
            editorService.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Returns all editors")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Editors found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Editor.class)) }),
    })
    @GetMapping
    public Iterable<Editor> readAll() { return editorService.readAll();}

    @Operation(summary = "Returns editor's data")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "invalid ID"),
            @ApiResponse(responseCode = "200", description = "Editor found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Editor.class)) }),
    })
    @GetMapping("/{id}")
    public Editor readOne(@PathVariable String id) {
        if(editorService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        return editorService.readById(id).get();
    }

    @Operation(summary = "Adds existing canvas to editor's collection")
    @PostMapping ("/{id}/editeByMe")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Canvas added to editor's collection", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = VectorCanvas.class)) }),
            @ApiResponse(responseCode = "404", description = "invalid ID"),
            @ApiResponse(responseCode = "409", description = "Editor already edits this canvas")
    })
    public void addCanvas (@PathVariable String id, @RequestBody VectorCanvas c) {

        if(vectorCanvasService.readById(c.getId()).isEmpty() || editorService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        try
        {
            editorService.addCanvas(id,c.getId());
        } catch (IllegalArgumentException e)
        {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @Operation(summary = "Removes canvas from editor's collection", description = "In case that canvas no longer belongs to any editor, then it deletes that canvas too.")
    @DeleteMapping ("/{id}/editeByMe/{canvasId}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Canvas removed from editor's collection"),
            @ApiResponse(responseCode = "404", description = "invalid ID")
    })
    public void removeCanvas (@PathVariable String id, @PathVariable String canvasId) {
       var vectorCanvas = vectorCanvasService.readById(canvasId);

        if ( vectorCanvas.isEmpty() || editorService.readById(id).isEmpty() )
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

        editorService.removeCanvas(id,canvasId);
    }
}
