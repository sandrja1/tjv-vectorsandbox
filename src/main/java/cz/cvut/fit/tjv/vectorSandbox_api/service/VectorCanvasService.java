package cz.cvut.fit.tjv.vectorSandbox_api.service;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import org.springframework.transaction.annotation.Transactional;

public interface VectorCanvasService extends CrudService<VectorCanvas,String> {

    void addShape ( String canvasId, long shapeId ) ;

    void deleteShape ( String canvasId, long shapeId ) ;
}
