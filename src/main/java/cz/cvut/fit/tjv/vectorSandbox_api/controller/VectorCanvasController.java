package cz.cvut.fit.tjv.vectorSandbox_api.controller;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Editor;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import cz.cvut.fit.tjv.vectorSandbox_api.service.EditorService;
import cz.cvut.fit.tjv.vectorSandbox_api.service.ShapeService;
import cz.cvut.fit.tjv.vectorSandbox_api.service.VectorCanvasService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;

@RestController
@CrossOrigin
@RequestMapping("/canvas")
public class VectorCanvasController {

    private VectorCanvasService canvasService;

    private ShapeService shapeService;

    private EditorService editorService;

    public VectorCanvasController(VectorCanvasService canvasService, ShapeService shapeService, EditorService editorService)
    {
        this.canvasService = canvasService;
        this.shapeService = shapeService;
        this.editorService = editorService;
    }

    @Operation(summary = "Creates a new canvas")
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "409", description = "Canvas with same name already exists!"),
            @ApiResponse(responseCode = "200", description = "Canvas created", content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = VectorCanvas.class)) })})
    public VectorCanvas create (@RequestBody VectorCanvas canvas ) {
        if ( canvasService.readById(canvas.getId()).isPresent())
            throw new ResponseStatusException(HttpStatus.CONFLICT);

        return canvasService.create(canvas);
    }

    @Operation(summary = "Adds new shape to canvas")
    @PostMapping("/{id}/shape")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "409", description = "Canvas already contains shape with exact definition!"),
            @ApiResponse(responseCode = "200", description = "Shape added",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Shape.class)) })
    })
    public Shape addShape (@PathVariable(value = "id") String canvasId, @RequestBody Shape s) {
        Shape shape = shapeService.create(s);

        try {
            canvasService.addShape(canvasId,shape.getId());
        }
        catch (IllegalArgumentException e)
        {
            shapeService.deleteById(shape.getId());
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }

        return shape;
    }

    @Operation(summary = "Deletes shaped in canvas")
    @DeleteMapping("/{id}/shape/{shapeId}")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Shape removed"),
            @ApiResponse(responseCode = "404", description = "invalid ID")
    })
    public void deleteShape (@PathVariable(value = "id") String canvasId, @PathVariable(value = "shapeId") Long shapeId)
    {
        try {
            canvasService.deleteShape(canvasId,shapeId);
        }
        catch (IllegalArgumentException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Returns all editor's usernames that not edit this canvas")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = ""),
            @ApiResponse(responseCode = "404", description = "invalid ID")
    })
    @GetMapping("{id}/notEditedBy")
    public Collection<String> getNotEditedBy (@PathVariable String id)
    {
        if(canvasService.readById(id).isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        return editorService.getEditorsNamesThatNotEditCanvas(canvasService.readById(id).get());
    }

    @Operation(summary = "Returns all canvases")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Canvases found",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = VectorCanvas.class)) }),
    })
    @GetMapping
    public Iterable<VectorCanvas> readAll() { return canvasService.readAll();}
}
