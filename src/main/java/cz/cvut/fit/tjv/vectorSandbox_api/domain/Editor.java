package cz.cvut.fit.tjv.vectorSandbox_api.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "editor")
public class Editor implements EntityWithId<String> {

    @Id
    private String username = "";

    @Column(name = "realname")
    private String realName ;

    @ManyToMany
    @JsonIgnore
    @JoinTable(
            name = "vector_canvas_editor",
            joinColumns = @JoinColumn(name = "editorname"),
            inverseJoinColumns = @JoinColumn(name = "canvasname")
    )
    private Collection<VectorCanvas> editedByMe;

    @Override
    public String getId() {
        return username;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Editor e) {
            return this.username.equals(e.username);
        } else
            return false;
    }

    @Override
    public int hashCode() {
        return this.username.hashCode();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Collection<VectorCanvas> getEditedByMe() {
        return editedByMe;
    }

    public void setEditedByMe(Collection<VectorCanvas> editedByMe) {
        this.editedByMe = editedByMe;
    }

    @JsonGetter("editedByMe")
    public Collection<String> getEditedByMeIds ( )
    {
        ArrayList<String> ids = new ArrayList<>();
        if(editedByMe == null)
            return new ArrayList<>();

        for ( var c : editedByMe)
        {
            ids.add(c.getId());
        }
        return ids;
    }
}
