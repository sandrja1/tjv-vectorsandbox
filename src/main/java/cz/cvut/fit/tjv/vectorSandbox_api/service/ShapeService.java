package cz.cvut.fit.tjv.vectorSandbox_api.service;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Shape;
import org.springframework.transaction.annotation.Transactional;

public interface ShapeService extends CrudService<Shape, Long>{
}
