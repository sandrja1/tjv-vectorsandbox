package cz.cvut.fit.tjv.vectorSandbox_api.repository;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Editor;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface EditorRepository extends CrudRepository<Editor, String> {

    @Query("SELECT username FROM Editor WHERE username NOT IN (SELECT e.username FROM Editor e WHERE :canvas member of e.editedByMe)")
    Collection<String> getEditorsNamesThatNotEditCanvas(VectorCanvas canvas);

    @Query("SELECT e.username FROM Editor e WHERE :canvas member of e.editedByMe")
    Collection<String> getEditorsNamesThatEditCanvas(VectorCanvas canvas);
}
