package cz.cvut.fit.tjv.vectorSandbox_api.service;

import cz.cvut.fit.tjv.vectorSandbox_api.domain.Editor;
import cz.cvut.fit.tjv.vectorSandbox_api.domain.VectorCanvas;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface EditorService extends CrudService<Editor, String> {

    void addCanvas (String editorId, String canvasId);

    void removeCanvas (String editorId, String canvasId);

    Collection<String> getEditorsNamesThatEditCanvas(VectorCanvas canvas);

    Collection<String> getEditorsNamesThatNotEditCanvas(VectorCanvas canvas);
}
