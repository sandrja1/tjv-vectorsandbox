# Vector Sandbox
`Editor` si bude moct vytvářet plátna `Canvas` a přidávat na ně různé vektorové tvary `Shape` případně i více editorů bude moct upravovat jedno plátno neboli spolupracovat. 

## Schéma
![Schema](DatabaseSchema.png)

## Dotaz
#### Formulace
Vyber všechny `Editor`, kteří needitují zadaný `Canvas`
#### Využití
Při přidávání spolueditora, je potřeba, aby byly na výběr pouze ti, kteří ho ještě needitují. 

## Komplexní operace
#### Popis
V rámci aplikace je potřeba zajistit unikátní jméno pro `Canvas`, aby nedocházelo ke kolizím.
#### Průběh
Při pokusu o vytvoření se nejdříve pošle požadavek na API, pro získání všech zabraných názvů,
pokud už existuje, uživateli to zahlásí a nepřidá se. Pokud je jméno volné, vytvoří se a pošle se požadavek na API pro uložení do databáze.

# Příprava a spuštění
## Instalace

Aplikace se skládá ze 2 částí: 
- API: https://gitlab.fit.cvut.cz/sandrja1/tjv-vectorsandbox
- Client: https://gitlab.fit.cvut.cz/sandrja1/tjv-vectorsandbox-client

Pro spuštění aplikace je potřeba mít stažené oba repozitáře a veškeré požadované nástroje:
### Požadavky 
- JVM
- Gradle
- Docker-compose

## Spuštění aplikace
### API
V adresáři `tjv-vectorsandbox`
```
docker-compose up
```
```
./gradlew bootRun
```
### Client
V adřesáři `tjv-vectorsandbox-client`
```
./gradlew bootRun
```
Aplikace bude dostupná na adrese: http://localhost:7090